<?php
if ( function_exists('has_nav_menu') && has_nav_menu('home-menu') ) { ?>
<?php
	wp_nav_menu( array( 'depth' => 6, 'sort_column' => 'menu_order', 'container' => 'ul', 'menu_class' => '', 'menu_id' => 'nav' , 'theme_location' => 'home-menu' ) );
?>
<?php } else {} ?>


<!DOCTYPE html>
<!--[if IE 6]>
<html id="ie6" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 7]>
<html id="ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html id="ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="utf-8"/>
	<title><?php global $page, $paged;
		wp_title('|', true, 'right');
		bloginfo('name');
		$site_description = get_bloginfo('description', 'display');
		echo " | $site_description";
		if ($paged >= 2 || $page >= 2) echo ' | ' . sprintf(__('Page %s', 'themnific'), max($paged, $page)); ?></title>

	<!-- Set the viewport width to device width for mobile -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>"/>

	<!-- load main css stylesheet -->
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_url'); ?>" media="screen"/>

	<!-- load prettyPhoto css -->
	<?php if (
		is_page_template('template-portfolio.php') ||
		is_page_template('template-portfolio-4col.php') ||
		is_home() ||
		is_page() ||
		is_archive() ||
		is_single()
	): ?>
		<link type="text/css" rel="stylesheet"
		      href="<?php echo get_template_directory_uri(); ?>/styles/prettyPhoto.css"/>
	<?php endif ?>


	<?php themnific_head(); ?>
	<?php wp_head(); ?>
</head>


<body <?php body_class(); ?>>


<div id="header" class="boxshadow">

	<div class="container" style="overflow:visible;">

		<a href="<?php echo home_url(); ?>/">
			<img class="logo" src="<?php echo get_template_directory_uri(); ?>/images/logo-marc-illien.png" alt="<?php bloginfo('name'); ?>"/>
		</a>

	</div>

</div>

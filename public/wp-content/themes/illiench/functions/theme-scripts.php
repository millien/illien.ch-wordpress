<?php
if ( ! is_admin() ) { add_action( 'wp_print_scripts', 'vergo_add_javascript' ); }

if ( ! function_exists( 'vergo_add_javascript' ) ) {
	function vergo_add_javascript() {

		// Load Common scripts	
		wp_enqueue_script('jquery');
		wp_enqueue_script('jquery.hoverIntent.minified', get_template_directory_uri().'/js/jquery.hoverIntent.minified.js');
		wp_enqueue_script('css3-mediaqueries', get_template_directory_uri().'/js/css3-mediaqueries.js');
		wp_enqueue_script('prettyPhoto', get_template_directory_uri() . '/js/jquery.prettyPhoto.js');
		wp_enqueue_script('jquery.scrollTo', get_template_directory_uri() . '/js/jquery.scrollTo.js');
		wp_enqueue_script('jquery.nav.', get_template_directory_uri() . '/js/jquery.nav.js');
		wp_enqueue_script('jquery.parallax-1.1.3', get_template_directory_uri() . '/js/jquery.parallax-1.1.3.js');
		wp_enqueue_script('superfish', get_template_directory_uri().'/js/superfish.js','','', true);
		wp_enqueue_script('jquery.hoverIntent.minified', get_template_directory_uri().'/js/jquery.hoverIntent.minified.js','','', true);
		wp_enqueue_script( 'jquery.adipoli.min', get_template_directory_uri().'/js/jquery.adipoli.min.js','','', true);
		wp_enqueue_script('ownScript', get_template_directory_uri() .'/js/ownScript.js','','', true);
		


		// Load single item slider scripts		
		if (is_home()) {
		wp_enqueue_script('jquery.flexslider-min', get_template_directory_uri() .'/js/jquery.flexslider-min.js');
		wp_enqueue_script('jquery.flexslider.start', get_template_directory_uri() .'/js/jquery.flexslider.start.js');
		}
		
		if ( is_singular()){
		wp_enqueue_script( 'comment-reply' );	
		wp_enqueue_script('jquery.flexslider-min', get_template_directory_uri() .'/js/jquery.flexslider-min.js');
		wp_enqueue_script('jquery.flexslider.start', get_template_directory_uri() .'/js/jquery.flexslider.start.js');
		}
		
	}
}
?>
                <footer class="footer" role="contentinfo">
			
				<div id="inner-footer" class="container clearfix">                                        
                                    <div class="footer-content">
					<p class="source-org copyright">&copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?>.</p>
                                    </div>
				</div> <!-- end #inner-footer -->
                                
		</footer> <!-- end footer -->
		
		</div> <!-- end #container -->
					
		<!-- all js scripts are loaded in library/bones.php -->
		<?php wp_footer(); ?>
</div>
	</body>

</html> <!-- end page. what a ride! -->

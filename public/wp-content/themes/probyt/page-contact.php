<?php
/*
Template Name: Contact 
*/
?>
<?php get_header(); ?>

<div id="content">

	<div id="inner-content" class="wrap clearfix">

		<div id="main" class="fourcol first clearfix" role="main">


			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article" itemscope
				         itemtype="http://schema.org/BlogPosting">

					<header class="article-header">

						<h1 class="page-title" itemprop="headline"><?php the_title(); ?></h1>

					</header>
					<!-- end article header -->

					<section class="entry-content clearfix" itemprop="articleBody">

						<?php the_content(); ?>

					</section>
					<!-- end article section -->


				</article> <!-- end article -->

			<?php endwhile; else : ?>

				<article id="post-not-found" class="hentry clearfix">
					<header class="article-header">
						<h1><?php _e("Oops, Post Not Found!", "bonestheme"); ?></h1>
					</header>
					<section class="entry-content">
						<p><?php _e("Uh Oh. Something is missing. Try double checking things.", "bonestheme"); ?></p>
					</section>
					<footer class="article-footer">
						<p><?php _e("This is the error message in the page.php template.", "bonestheme"); ?></p>
					</footer>
				</article>

			<?php endif; ?>

		</div>
		<!-- end #main -->

		<div class="eightcol sidebar-enventa">
			<iframe width="100%" height="600" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.ch/maps?f=q&amp;source=s_q&amp;hl=de&amp;geocode=&amp;q=Probyt+AG,+Gaswerkstrasse+22B,+Frauenfeld&amp;aq=1&amp;oq=probyt&amp;sll=46.813187,8.22421&amp;sspn=2.218145,3.279419&amp;ie=UTF8&amp;hq=Probyt+AG,&amp;hnear=Gaswerkstrasse+22B,+8500+Frauenfeld,+Thurgau&amp;t=m&amp;ll=47.563786,8.914719&amp;spn=0.138998,0.257149&amp;z=12&amp;iwloc=A&amp;output=embed"></iframe><br /><small><a href="https://maps.google.ch/maps?f=q&amp;source=embed&amp;hl=de&amp;geocode=&amp;q=Probyt+AG,+Gaswerkstrasse+22B,+Frauenfeld&amp;aq=1&amp;oq=probyt&amp;sll=46.813187,8.22421&amp;sspn=2.218145,3.279419&amp;ie=UTF8&amp;hq=Probyt+AG,&amp;hnear=Gaswerkstrasse+22B,+8500+Frauenfeld,+Thurgau&amp;t=m&amp;ll=47.563786,8.914719&amp;spn=0.138998,0.257149&amp;z=12&amp;iwloc=A" style="color:#0000FF;text-align:left">Größere Kartenansicht</a></small>
		</div>
		<!-- end #inner-content -->

	</div>
	<!-- end #content -->

	<?php get_footer(); ?>

<?php
/*
Template Name: Enventa 
*/
?>
<?php get_header(); ?>

<div id="content">

	<div id="inner-content" class="wrap clearfix">

		<div id="main" class="eightcol first clearfix" role="main">


			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article" itemscope
				         itemtype="http://schema.org/BlogPosting">

					<header class="article-header">

						<h1 class="page-title" itemprop="headline"><?php the_title(); ?></h1>

					</header>
					<!-- end article header -->

					<section class="entry-content clearfix" itemprop="articleBody">

						<?php the_content(); ?>

						<div class="enventa-footer">
							<h2>Module</h2>
							<ul class="enventa-module-list">
								<?php
								$args = array('post_type' => 'enventa_module', 'numberposts' => 20, 'orderby' => 'menu_order', 'order' => 'ASC');
								$rand_posts = get_posts($args);
								foreach ($rand_posts as $post) : ?>
									<li>
										<a class="module_link" href="<?php the_permalink(); ?>">
											<?php
											$env_image = get_post_meta($post->ID, "modul_image", true);
											$env_image_markup = wp_get_attachment_image_src($env_image);
											?>
											<div class="mod_img">
												<?php echo "<img src='" . $env_image_markup[0] . "' />" ?>
											</div>
											<div class="mod_title"><?php the_title(); ?></div>
											<div class="mod_add">
												<span class="mod_desc">
													<?php the_excerpt(); ?>
													<span class="mod_more">mehr erfahren</span>
												</span>

											</div>
										</a>
									</li>
								<?php endforeach; ?>
							</ul>
							<p>
								<a target="_blank" href="/wp-content/uploads/2013/02/eNVenta-ERP_Kurzbroschüre.pdf">eNVenta ERP Broschüre als PDF ansehen</a>
							</p>
						</div>
						<div class="clear"></div>

					</section>
					<!-- end article section -->

					<footer class="article-footer">
						<?php the_tags('<span class="tags">' . __('Tags:', 'bonestheme') . '</span> ', ', ', ''); ?>
					</footer>
					<!-- end article footer -->

				</article> <!-- end article -->

			<?php endwhile; else : ?>

				<article id="post-not-found" class="hentry clearfix">
					<header class="article-header">
						<h1><?php _e("Oops, Post Not Found!", "bonestheme"); ?></h1>
					</header>
					<section class="entry-content">
						<p><?php _e("Uh Oh. Something is missing. Try double checking things.", "bonestheme"); ?></p>
					</section>
					<footer class="article-footer">
						<p><?php _e("This is the error message in the page.php template.", "bonestheme"); ?></p>
					</footer>
				</article>

			<?php endif; ?>

		</div>
		<!-- end #main -->

		<?php get_sidebar() ?>

		<div class="clear"></div>

	</div>
	<!-- end #inner-content -->

</div> <!-- end #content -->

<?php get_footer(); ?>

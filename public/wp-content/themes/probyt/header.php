<!doctype html>

<!--[if lt IE 7]>
<html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if (IE 7)&!(IEMobile)]>
<html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]>
<html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!-->
<html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->

<head>
	<meta charset="utf-8">
	<title><?php wp_title(''); ?></title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="HandheldFriendly" content="True">
	<meta name="MobileOptimized" content="320">
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
	<!--[if lt IE 9]>
	<script src="/wp-content/themes/probyt/library/js/libs/html5shiv.js"></script>
	<![endif]-->
	<?php wp_head(); ?>

	<script>
		(function (i, s, o, g, r, a, m) {
			i['GoogleAnalyticsObject'] = r;
			i[r] = i[r] || function () {
				(i[r].q = i[r].q || []).push(arguments)
			}, i[r].l = 1 * new Date();
			a = s.createElement(o),
				m = s.getElementsByTagName(o)[0];
			a.async = 1;
			a.src = g;
			m.parentNode.insertBefore(a, m)
		})(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

		ga('create', 'UA-41265583-1', 'probyt.ch');
		ga('send', 'pageview');

	</script>

</head>

<body <?php body_class(); ?>>

<div class="wrap">
	<div id="container">

		<div class="bordertop"></div>

		<header class="header" role="banner">

			<div id="inner-header" class="wrap clearfix">

				<div class="ninecol first">
					<div class="fourcol first" id="col-logo">
						<a href="<?php echo home_url(); ?>" rel="nofollow">
							<img alt="ProBYT Logo" src="<?php echo get_template_directory_uri(); ?>/library/images/logo.png"/>
						</a>
					</div>
					<div class="sevencol" id="col-slogan">
						<div class="site-slogan"><?php bloginfo('description'); ?></div>
					</div>
				</div>

				<div class="threecol last col-address">

					<div class="twelvecol first">

						<div class="address-inside">
							<div class="address first">
								<strong>PRO<span class="red">BYT</span> Systems AG</strong><br/>
								Gaswerkstrasse 22b<br/>8500 Frauenfeld<br/>
								<span id="telm">Tel. +41(0) 52 728 99 88<br/>
								<a target="_blank" href="mailto:info@probyt.ch">E-Mail</a></span>
							</div>
							<div class="address" id="address-nr">
								Tel. +41(0) 52 728 99 88
								<br/>
								<a target="_blank" href="mailto:info@probyt.ch">E-Mail</a>
							</div>
						</div>
					</div>

				</div>

				<div class="clearfix"></div>

			</div>
			<!-- end #inner-header -->

		</header>
		<!-- end header -->

		<div class="header-image">
			<div class="header-slogan">
				<?php
				global $post;
				$out = "Freiheit geniessen";
				$customslogan = get_field("headerslogan", $post->ID);
				print $customslogan != "" ? $customslogan : $out;
				?>
			</div>
			<?php if (has_post_thumbnail()) : ?>
				<?php the_post_thumbnail('bones-thumb-960'); ?>
			<?php else : ?>
				<img alt="" width="960" height="120"
				     src="<?php echo get_template_directory_uri(); ?>/library/images/content/skydive1.jpg"/>
			<?php endif; ?>
		</div>

		<nav role="navigation">
			<div id="main-navigation">
				<?php bones_main_nav(); ?>
			</div>
		</nav>


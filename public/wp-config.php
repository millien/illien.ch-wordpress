<?php


// define setting vars
define('CONFIG_PATH', dirname(__FILE__));

$server_env = "loc";
$config_path = CONFIG_PATH;
$default_config = "wp-config-loc.php";
$host = $_SERVER['HTTP_HOST'];

switch ($host) {
	case "illien.dev":
		$default_config = "wp-config-loc.php";
		break;
	case "illien.ch":
		$default_config = "wp-config-prod.php";
		break;
	case "qa.illien.ch":
		$default_config = "wp-config-qa.php";
		break;
	default:
		$default_config = "wp-config-prod.php";
		break;
}

// Load config
require_once(realpath(__DIR__.'/..').'/config/' . $default_config);
